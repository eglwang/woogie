
#include <string>
#include <random>
#include <sstream>
#include <openssl/md5.h>
#include "utility.h"
using namespace std;

unsigned char *get_md5_of(const char *str, int len)
{
    unsigned char *md5 = new unsigned char[MD5_DIGEST_LENGTH];
    MD5_CTX ctx;
    MD5_Init(&ctx);
    MD5_Update(&ctx, str, len);
    MD5_Final(md5, &ctx);
    return md5;
}

string get_md5_str(const char *str, int len)
{
    unsigned char *md5 = get_md5_of(str, len);
    stringstream ss;
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        ss << hex << (int)md5[i];
    }
    delete[] md5;
    return ss.str();
}
string random_string(int length)
{
    string str(length, ' ');
    static const char alphabet[] =
        "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "0123456789";
    static std::default_random_engine rng(std::random_device{}());
    static std::uniform_int_distribution<> dist(0, sizeof(alphabet) - 2);

    for (int i = 0; i < length; ++i)
    {
        str[i] = alphabet[dist(rng)];
    }

    return str;
}
