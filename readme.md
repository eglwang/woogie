# About the project *Woogie*

A convenient tool to make a pseudo terminal and interface with it remotely via MQTT(Baidu-IoT-core exactly). This can used to control you linux/unix devices remotely acrossing LANs over a telnet client.

>not for Windows devices.  
at least two keys are necessary to connect the MQTT broker.  
controlling you device over a telnet client(available on Windows), so only a cmd-line interface is provided.

---

## Install

Make sure [paho.mqtt.c](https://github.com/eclipse/paho.mqtt.c.git) and openSSL library is installed in your system before install Woogie.  
Install paho.mqtt.c in your system according the orders form paho.mqtt.c's documents. Note that only paho-mqtt3a is necessary.   

### install Woogie through cmake
```sh
cd woogie
mkdir build
cd build
cmake ..
cmake --build . # build all targets
```


## How to use?

1. create a config file named `mqtt.ini` in the machine you want to control(host machine), and make sure it looks like this:

```ini
[Jitterbug]
client_id=****
password=***********
client_password=YourPasswordToLoginThroughTelnet
```
> please contact the author(eglwang@163.com) if you need keys to use this tool. BTW, two accounts are needed to connect the MQTT broker here.
2. Then run `./build/Jitterbug [<path to your config file>]`, this will connects to MQTT broker, and listening corresponding topics.  
Or, in a better way, you may want to create a service unit to run this specifically as a service, we provided a unit file(`Jitterbug.service`) for you, be ware that to replace the paths in the file with yours.


3. Run `./build/Belinda [<port>] [<path to your config file>]` in your machine,this will create a telnet server, you can specify the port of service if needed, `./build/Belinda 8080` example will open the service in port 8080. Note that you need a config file named `mqtt.ini` as well, which may looks like this:

```ini
[mqtt]
client_id =*****
password =**********
target =*****    ; this is the device_id of your host machine
client_password=YourPasswordToLoginThroughTelnet # optional
```


4. run the telnet client. `telnet localhost`, by default the *echo* feature is turned off, use `stty echo` to turn it off. Alternately you can use *mode character* in telnet, which is useful in most occasions.

---

## Miscellaneous

Here are an example to run Woogie, this example run both Jitterbug and Belinda on the same machine.  
The directory looks like this:
```
Woogie
├── mqtt.ini
└── build
    ├── Jitterbug
    └── Belinda
```
We provide a free conifg file which contains two MQTT testing accounts.

```ini
[mqtt]
client_id =dev1002
password =0d8a58dfc5dd756c3f6560bbe3ad037b
target =dev2
client_password =123456

[Jitterbug]
client_id=dev2
password=d499fd973a615eee4a7685785d24b8dd
keep_alive=30
client_password=123456
```
Run the following cmds:

```sh
cd woogie
cd build
# run Jitterbug in this PC
./Jitterbug&
# the following operations can also be run on another PC
# start the telnet server
./Belinda 8080&
# connect Jitterbug through telnet
telnet localhost 8080

```
It outputs like this:

```
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
client 127.0.0.1:50982 connected
MQTT Broker Connected
 TTL: 409 ms 
 TTL: 409 ms 
[Login]

ubuntu@xxx:/usr/share/woogie $
```
So far you can control that terminal remotely, note that this example runs Jitterbug and Belinda on a same PC, you can run them separately on different machines, that what them were designed.

There'are some glitches in enable or disable Local Echo in telnet so far, and it displayed some content abnormally sometimes.