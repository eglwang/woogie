#ifndef __MQTT_CONFIG_H__
#define __MQTT_CONFIG_H__
#include "testsuit.h"
#include <iostream>
using namespace std;

// error codes, used in on_error_callback_t
typedef enum
{
    MQTT_Connection_OK = 0,
    MQTT_Connection_Lost = 1,
    MQTT_Connection_Failure = 2,
} MQTT_Connection_Error_Code;

// read and init the basic config as a master or client
int read_config(string &f_name);
// read and init the basic config as a slaver, used in jc-pi
int read_config_slaver(string &f_name);
int start_loop();
int stop_loop();
int mqtt_out_raw(string &msg);
int mqtt_out_raw(const char *msg, int len);

typedef void (*on_message_callback_t)(char *topicName, int topicLen, char *payload, int payloadlen);
typedef void (*on_error_callback_t)(int error_code);

void set_on_message_callback(on_message_callback_t cb);
void set_on_error_callback(on_error_callback_t cb);
int client_publish(string &topic, string &msg);
int client_publish(const char *topic, string &msg);
int client_publish(string &topic, const char *msg, int len);
int client_publish(const char *topic, const char *msg, int len);
int subscribe_topic(string &topic);
string get_client_password();
string get_client_id();
#endif