#ifndef __CONFIG_PARSER_H__
#define __CONFIG_PARSER_H__
#include "testsuit.h"
#include <sstream>
#include <unordered_map>
#include <string>

typedef std::unordered_map<std::string, std::unordered_map<std::string, std::string> > config_map_t;
std::string config_get(config_map_t &config_map, const std::string &section, const std::string &option, const std::string fallback = "");
config_map_t &config_parse(std::string &file_name);
void free_config_map(config_map_t &config_map);


#endif