#ifndef __JENNY_H__
#define __JENNY_H__
#include "testsuit.h"
#include <iostream>
void set_pty_size(unsigned short rows, unsigned short cols);
void set_pty_echo(bool enable);
void start_pty(void (*out_function)(char *buf, int len));

void input_zxinto_pty(std::string str);
void input_into_pty(const char *str, int length);
void input_into_pty_no_newline(std::string str);
void input_into_pty_no_newline(const char *str, int len);
bool check_subprocess(); // return true if the subprocess is exited
void close_pty();

#endif