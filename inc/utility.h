#ifndef __UTILITY_H__
#define __UTILITY_H__
#include <iostream>
unsigned char *get_md5_of(const char *str, int len);
std::string get_md5_str(const char *str, int len);
std::string random_string(int length);
#endif