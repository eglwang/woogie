#ifndef __BELINDA_H__
#define __BELINDA_H__
#include "testsuit.h"

void _filter_data_from_sock(char *data, int len);
void _on_auth(char *data, int len);
void send_by_socket(const char *msg, int len, char color = 0);
void send_by_socket(string &msg, char color = 0);
void send_by_socket_s(const char *msg, char color = 0);

#endif